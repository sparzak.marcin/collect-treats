const express = require(`express`);
const consola = require(`consola`);
const { Nuxt, Builder } = require(`nuxt`);
const app = express();

const firebase = require(`firebase/app`);
const firebaseConfig = {
  apiKey: `AIzaSyAQe6piCB3h7DIobLpImMyreXTuoTs-K8M`,
  authDomain: `collecttreats.firebaseapp.com`,
  projectId: `collecttreats`,
  storageBucket: `collecttreats.appspot.com`,
  messagingSenderId: `237778064707`,
  appId: `1:237778064707:web:3dd30904b0874d0d1e0bf6`
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

// Import and Set Nuxt.js options
const config = require(`../nuxt.config.js`);
config.dev = process.env.NODE_ENV !== `production`;

async function start() {
  // Init Nuxt.js
  const nuxt = new Nuxt(config);

  const { host, port } = nuxt.options.server;

  await nuxt.ready();
  // Build only in dev mode
  if (config.dev) {
    const builder = new Builder(nuxt);
    await builder.build();
  }

  // Give nuxt middleware to express
  app.use(nuxt.render);

  // Listen the server
  app.listen(port, host);
  consola.ready({
    message: `Server listening on http://${host}:${port}`,
    badge: true
  });
}
start();
