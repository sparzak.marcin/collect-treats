import Phaser from "phaser";
import { Apple } from "../entities/apple/Apple.js";
export class Collectables extends Phaser.Physics.Arcade.StaticGroup {
  constructor(scene) {
    super(scene.physics.world, scene);

    this.createFromConfig({
      classType: Apple
    });
  }

  mapProperties(propertyList) {
    if (propertyList && propertyList.length > 0) {
      return propertyList.reduce((map, obj) => {
        map[obj.name] = obj.value;
        return map;
      }, {});
    } else {
      return {};
    }
  }

  addFromLayer(layer) {
    const { score: defaultScore, type } = this.mapProperties(layer.properties);
    layer.objects.forEach(collectableObject => {
      const collectable = this.get(collectableObject.x, collectableObject.y, type);
      const props = this.mapProperties(collectableObject.properties);
      collectable.score = props.score || defaultScore;
    });
  }
}
