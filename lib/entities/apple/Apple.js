import Phaser from "phaser";

export class Apple extends Phaser.Physics.Arcade.Sprite {
  constructor(scene, x, y) {
    super(scene, x, y, `apple`);
    scene.add.existing(this);

    this.setOrigin(0, 1);

    scene.tweens.add({
      targets: this,
      y: this.y - Phaser.Math.Between(3, 10),
      duration: Phaser.Math.Between(1500, 2500),
      repeat: -1,
      easy: `linear`,
      yoyo: true
    });
  }
}
