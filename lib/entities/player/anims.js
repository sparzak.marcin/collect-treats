function createAnims(context) {
  context.anims.create({
    key: `dog-idle`,
    frames: [
      { key: `player_dog_idle_1` },
      { key: `player_dog_idle_2` },
      { key: `player_dog_idle_3` },
      { key: `player_dog_idle_4` },
      { key: `player_dog_idle_5` },
      { key: `player_dog_idle_6` },
      { key: `player_dog_idle_7` },
      { key: `player_dog_idle_8` },
      { key: `player_dog_idle_9` },
      { key: `player_dog_idle_10` }
    ],
    frameRate: 5,
    repeat: -1
  });
  context.anims.create({
    key: `dog-run`,
    frames: [
      { key: `player_dog_run_1` },
      { key: `player_dog_run_2` },
      { key: `player_dog_run_3` },
      { key: `player_dog_run_4` },
      { key: `player_dog_run_5` },
      { key: `player_dog_run_6` },
      { key: `player_dog_run_7` },
      { key: `player_dog_run_8` }
    ],
    frameRate: 10,
    repeat: -1
  });
  context.anims.create({
    key: `dog-jump`,
    frames: [
      { key: `player_dog_jump_1` },
      { key: `player_dog_jump_2` },
      { key: `player_dog_jump_3` },
      { key: `player_dog_jump_4` },
      { key: `player_dog_jump_5` },
      { key: `player_dog_jump_6` },
      { key: `player_dog_jump_7` },
      { key: `player_dog_jump_8` }
    ],
    frameRate: 10,
    repeat: 0
  });
  context.anims.create({
    key: `dog-fall`,
    frames: [
      { key: `player_dog_fall_1` },
      { key: `player_dog_fall_2` },
      { key: `player_dog_fall_3` },
      { key: `player_dog_fall_4` },
      { key: `player_dog_fall_5` },
      { key: `player_dog_fall_6` },
      { key: `player_dog_fall_7` },
      { key: `player_dog_fall_8` }
    ],
    frameRate: 10,
    repeat: 0
  });
}

export default createAnims;
