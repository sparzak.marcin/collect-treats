import Phaser from "phaser";
import createAnims from "./anims";
import playerConfiguration from "./configuration";
class Player extends Phaser.Physics.Arcade.Sprite {
  constructor(scene, x, y, key) {
    super(scene, x, y, key);

    scene.add.existing(this);
    scene.physics.add.existing(this);
    this.init();
    this.initEvents();
  }

  init() {
    this.setScale(0.1, 0.1);
    this.setSize(this.width - 350, this.height - 40);
    // this.setOffset(50, 0);
    this.setOrigin(0.5, 1);
    this.body.setGravityY(500);
    this.setCollideWorldBounds(true);
    this.cursors = this.scene.input.keyboard.createCursorKeys();
    createAnims(this);

    this.initSounds();

    this.isFalling = false;
  }

  initSounds() {
    this.jumpSound = this.scene.sound.add(`jump`, { volume: 0.7 });
    this.runningSound = this.scene.sound.add(`running`, { volume: 0.1 });
    this.scene.time.addEvent({
      delay: 300,
      repeat: -1,
      callbackScope: this,
      callback: () => {
        if (this.anims.getName() === `dog-run`) {
          this.runningSound.play();
        }
      }
    });
  }

  initEvents() {
    this.scene.events.on(Phaser.Scenes.Events.UPDATE, this.update, this);
  }

  update() {
    if (this.body) {
      const onFloor = this.body.onFloor();
      const { left, right, space } = this.cursors;
      if (left.isDown) {
        this.setVelocityX(-playerConfiguration.speed);
        this.setFlipX(true);
      } else if (right.isDown) {
        this.setVelocityX(playerConfiguration.speed);
        this.setFlipX(false);
      } else {
        this.setVelocityX(0);
      }

      const isSpaceJustDown = Phaser.Input.Keyboard.JustDown(space);
      if (isSpaceJustDown && onFloor) {
        this.jumpSound.play();
        this.setVelocityY(-playerConfiguration.jumpVelocity);
      }
      if (onFloor) {
        this.isFalling = false;
        if (this.body.velocity.x !== 0) {
          this.play(`dog-run`, true);
        } else {
          this.play(`dog-idle`, true);
        }
      } else if (this.body.velocity.y < 0) {
        this.play(`dog-jump`, true);
      } else if (!this.isFalling && this.body.velocity.y > 0) {
        this.isFalling = true;
        this.play(`dog-fall`, true);
      }
    }
  }
}

export default Player;
