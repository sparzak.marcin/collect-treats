import Phaser from "phaser";

class ScoreHud extends Phaser.GameObjects.Container {
  constructor(scene, x, y) {
    super(scene, x, y);
    scene.add.existing(this);

    this.fontSize = 35;
    const { rightTopCorner } = scene.config;
    this.setPosition(rightTopCorner.x - 80, rightTopCorner.y + 10);
    this.setScrollFactor(0);

    const scoreBoard = this.createScoreboard();
    scoreBoard.setName(`scoreBoard`);
    this.add(scoreBoard);
  }

  createScoreboard() {
    const scoreImage = this.scene.add
      .image(0, 0, `apple`)
      .setOrigin(0)
      .setScale(2);
    const scoreText = this.scene.add.text(35, 0, `0`, {
      fontSize: `${this.fontSize}px`,
      fill: `#000`
    });

    return this.scene.add.container(0, 0, [scoreText, scoreImage]);
  }

  updateScore(score) {
    const [scoreText] = this.getByName(`scoreBoard`).list;
    scoreText.setText(score);
  }
}

export default ScoreHud;
