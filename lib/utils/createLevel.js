// eslint-disable-next-line no-unused-vars
import Phaser from "phaser";
const createLevel = scene => {
  const currentLevel = scene.registry.get(`level`) || 1;

  const map = scene.make.tilemap({
    key: `level_${currentLevel}`
  });
  if (map.layers.length) {
    const layers = {};

    const tileSets = {};

    switch (currentLevel) {
      case 1:
        tileSets.main = map.addTilesetImage(`main_lev_build_1`, `tile_set_1`);

        layers.colliders = map.createLayer(`Colliders`, tileSets.main);
        layers.colliders.setCollisionByExclusion(-1, true);

        layers.platforms = map.createLayer(`Platforms`, tileSets.main);

        layers.environment = map.createLayer(`Environment`, tileSets.main);

        break;
      case 2:
      case 3:
      case 4:
      case 5:
        tileSets.hills = map.addTilesetImage(`hills`, `hills_tile_set_2`);
        // tileSets.props2 = map.addTilesetImage(`Props2`, `hills_props`);
        tileSets.props = map.addTilesetImage(`Props`, `hills_props`);

        layers.environment = map.createLayer(`Environment`, [tileSets.props, tileSets.hills]);
        layers.colliders = map.createLayer(`Colliders`, tileSets.hills);
        layers.colliders.setCollisionByExclusion(-1, true);

        break;
      case 6:
      case 7:
      case 8:
      case 9:
        tileSets.forest = map.addTilesetImage(`forest`, `forest_tileset`);
        layers.environment = map.createLayer(`Environment`, tileSets.forest).setDepth(-2);
        layers.environment = map.createLayer(`Hidden`, tileSets.forest).setDepth(4);
        layers.colliders = map.createLayer(`Colliders`, tileSets.forest);
        layers.colliders.setCollisionByExclusion(-1, true);
        layers.colliders.setAlpha(1);
        layers.grass = map.createLayer(`Grass`, tileSets.forest);
        layers.bg = [];
        // eslint-disable-next-line no-case-declarations
        const [bg1] = map.getObjectLayer(`bg1`).objects;
        layers.bg.push(
          scene.add
            .tileSprite(bg1.x, bg1.y, scene.config.width, bg1.height, `pine2`)
            .setOrigin(0, 1)
            .setDepth(-10)
            .setScrollFactor(0, 1)
        );
        // eslint-disable-next-line no-case-declarations
        const [bg2] = map.getObjectLayer(`bg2`).objects;
        layers.bg.push(
          scene.add
            .tileSprite(bg2.x, bg2.y, scene.config.width, bg2.height, `pine1`)
            .setOrigin(0, 1)
            .setDepth(-11)
            .setScrollFactor(0, 1)
        );
        // eslint-disable-next-line no-case-declarations
        const [bg3] = map.getObjectLayer(`bg3`).objects;
        layers.bg.push(
          scene.add
            .tileSprite(bg3.x, bg3.y, scene.config.width, bg3.height, `mountain2`)
            .setOrigin(0, 1)
            // .setScale(2)
            .setDepth(-12)
            .setScrollFactor(0, 1)
        );
        // eslint-disable-next-line no-case-declarations
        const [bg4] = map.getObjectLayer(`bg4`).objects;
        layers.bg.push(
          scene.add
            .tileSprite(bg4.x, bg4.y, scene.config.width, bg4.height, `sky`)
            .setScale(1, 1.6)
            .setOrigin(0, 1)
            .setDepth(-13)
            .setScrollFactor(0, 1)
        );
        break;
      default:
        break;
    }
    // const tileSet2 = map.addTilesetImage(`main_lev_build_2`, `tile_set_2`);
    // debugger;

    const playerZones = map.getObjectLayer(`player_zones`);
    const playerZonesObjects = getPlayerZoneObjects(playerZones);
    layers.collectables = map.getObjectLayer(`collectables`);

    const level = {
      map,
      layers,
      playerZonesObjects,
      levelNr: currentLevel
    };
    return level;
  } else {
    return false;
  }
};

const getPlayerZoneObjects = layer => {
  // debugger;
  const result = {
    startZone: null,
    endZone: null
  };
  const { objects } = layer;
  objects.forEach(element => {
    switch (element.name) {
      case `start_zone`:
        result.startZone = element;
        break;
      case `end_zone`:
        result.endZone = element;
        break;
      default:
        break;
    }
  });
  return result;
};

export default createLevel;
