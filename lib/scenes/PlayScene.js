import Phaser from "phaser";
import createLevel from "../utils/createLevel";
import Player from "../entities/player/Player";
import { Collectables } from "../groups/Collectables";
import ScoreHud from "../hud/score";
class PlayScene extends Phaser.Scene {
  constructor(config) {
    super(`PlayScene`);
    this.config = config;
  }

  create() {
    this.score = 0;
    // eslint-disable-next-line no-unused-vars
    const level = createLevel(this);
    this.levelNr = level.levelNr;
    if (!level) {
      this.scene.start(`Menu`);
      return;
    }
    if (!this.sound.get(`theme`)) {
      this.sound.add(`theme`, { volume: 0.1, loop: true }).play();
    }
    const { map, layers, playerZonesObjects } = level;
    this.layers = layers;
    // Player;
    this.player = new Player(this, playerZonesObjects.startZone.x, playerZonesObjects.startZone.y, `player_dog_idle_1`);
    this.physics.add.collider(this.player, layers.colliders);

    // End of Level
    this.createEndZone(playerZonesObjects.endZone, this.player);

    // Collectables
    this.collectSound = this.sound.add(`collect`, { volume: 0.1 });
    if (layers.collectables) {
      this.collectables = new Collectables(this);
      this.collectables.addFromLayer(layers.collectables);
      this.physics.add.overlap(this.player, this.collectables, this.onCollect, null, this);
    }

    // Camera
    this.setUpFollowupCameraOn(this.player, map);

    this.createBG(layers.bg);
    this.createBackButton();

    this.scoreHud = new ScoreHud(this, 0, 0);

    this.events.on(`resume`, () => {
      this.physics.resume();
    });
  }

  onCollect(player, collectable) {
    this.collectSound.play();
    this.score += collectable.score;
    collectable.disableBody(true, true);
    this.scoreHud.updateScore(this.score);
  }

  createBG(bgLayers) {
    if (!bgLayers || !bgLayers.length) {
      this.skyImage = this.add
        .tileSprite(0, 0, this.config.width, 270, `bg_orange`)
        .setOrigin(0, 0)
        .setScale(2.5)
        .setDepth(-16)
        .setScrollFactor(0, 1);
    }
  }

  setUpFollowupCameraOn(player, map) {
    const { height, zoomFactor } = this.config;
    // debugger;
    this.physics.world.setBounds(0, 0, map.widthInPixels, height + 200);
    this.cameras.main.setBounds(0, 0, map.widthInPixels, height).setZoom(zoomFactor);
    this.cameras.main.startFollow(player, false, 1, 1, 0, 50);
  }

  createBackButton() {
    const btn = this.add
      .image(this.config.rightBottomCorner.x, this.config.rightBottomCorner.y, `back`)
      .setOrigin(1)
      .setScrollFactor(0)
      .setInteractive();
    btn.on(`pointerup`, () => {
      this.physics.pause();
      this.scene.pause();
      this.scene.launch(`Menu`, { pause: true });
    });
  }

  createEndZone(endZoneObj, player) {
    const endZone = this.physics.add
      .sprite(endZoneObj.x, endZoneObj.y, `endZone`)
      .setAlpha(0)
      .setSize(5, this.config.height)
      .setOrigin(0.5, 1);
    const endZoneOverlap = this.physics.add.overlap(player, endZone, () => {
      console.log(`end`);
      this.registry.set(`level`, this.levelNr + 1);
      if (this.levelNr < this.config.maxLevel) {
        this.registry.set(`unlocked-levels`, this.levelNr + 1);
      }
      endZoneOverlap.active = false;
      this.scene.restart();
    });
  }

  update() {
    // paralax
    if (this.layers.bg && this.layers.bg.length) {
      let paralaxValue = 0.5;
      for (let index = 0; index < this.layers.bg.length; index++) {
        const bg = this.layers.bg[index];
        bg.tilePositionX = this.cameras.main.scrollX * paralaxValue;
        paralaxValue = paralaxValue === 0.1 ? paralaxValue : paralaxValue - 0.1;
      }
    } else {
      this.skyImage.tilePositionX = this.cameras.main.scrollX * 0.1;
    }
  }
}
export default PlayScene;
