import Phaser from "phaser";

export class Menu extends Phaser.Scene {
  constructor(config) {
    super(`Menu`);
    this.mainMenu = [
      {
        text: `Nowa gra`,
        action: () => {
          this.registry.set(`level`, 1);
          this.scene.start(`PlayScene`);
        }
      },
      {
        text: `Wybierz level`,
        action: () => {
          this.scene.start(`LevelsScene`);
        }
      },
      {
        action: () => {
          console.log(`wyjdź z gry`);
        },
        text: `Wyjdź`
      }
    ];
    this.pauseMenu = [
      {
        action: () => {
          this.scene.stop();
          this.scene.resume(`PlayScene`);
        },
        text: `Kontynuj`
      },
      {
        action: () => {
          this.scene.stop(`PlayScene`);
          this.scene.restart({ pause: false });
        },
        text: `Wyjdź do menu`
      }
    ];
    this.config = config;
  }

  create(data) {
    const tmpMenu = data.pause ? this.pauseMenu : this.mainMenu;
    this.add
      .image(0, 0, `bg_night`)
      .setOrigin(0)
      .setScale(5.7);

    let lastPositionY = 0;

    tmpMenu.forEach(menuItem => {
      const menuPosition = [this.config.width / 2, this.config.height / 2 + lastPositionY];
      menuItem.object = this.add
        .text(...menuPosition, menuItem.text, { fontSize: `32px`, fill: `#fff` })
        .setOrigin(0.5, 1);

      menuItem.object.setInteractive();
      menuItem.object.on(`pointerover`, () => {
        menuItem.object.setStyle({ fill: `#ff0` });
      });
      menuItem.object.on(`pointerout`, () => {
        menuItem.object.setStyle({ fill: `#fff` });
      });

      menuItem.object.on(`pointerup`, menuItem.action);
      lastPositionY += 42;
    });
  }
}
