import Phaser from "phaser";
class Preload extends Phaser.Scene {
  constructor() {
    super(`Preload`);
  }

  preload() {
    this.load.tilemapTiledJSON(`level_1`, `levelMaps/level_1.json`);
    this.load.tilemapTiledJSON(`level_2`, `levelMaps/level_2.json`);
    this.load.tilemapTiledJSON(`level_3`, `levelMaps/level_3.json`);
    this.load.tilemapTiledJSON(`level_4`, `levelMaps/level_4.json`);
    this.load.tilemapTiledJSON(`level_5`, `levelMaps/level_5.json`);
    this.load.tilemapTiledJSON(`level_6`, `levelMaps/level_6.json`);
    this.load.tilemapTiledJSON(`level_7`, `levelMaps/level_7.json`);
    this.load.tilemapTiledJSON(`level_8`, `levelMaps/level_8.json`);
    this.load.tilemapTiledJSON(`level_9`, `levelMaps/level_9.json`);
    this.load.image(`tile_set_1`, `tilesets/main_lev_build_1.png`);
    this.load.image(`tile_set_2`, `tilesets/main_lev_build_2.png`);

    // hills
    this.load.image(`hills_tile_set_2`, `tilesets/hills/Tileset(16x16)/Tileset.png`);
    this.load.image(`hills_props`, `tilesets/hills/Props/Props.png`);

    // forrest
    this.load.image(`forest_tileset`, `tilesets/forest/forest-ext.png`);
    this.load.image(`pine2`, `tilesets/forest/Background/pine2.png`);
    this.load.image(`pine1`, `tilesets/forest/Background/pine1.png`);
    this.load.image(`mountain2`, `tilesets/forest/Background/mountain2.png`);
    this.load.image(`sky`, `tilesets/forest/Background/sky.png`);

    this.load.image(`player_dog_idle_1`, `player/dog/Idle (1).png`);
    this.load.image(`player_dog_idle_2`, `player/dog/Idle (2).png`);
    this.load.image(`player_dog_idle_3`, `player/dog/Idle (3).png`);
    this.load.image(`player_dog_idle_4`, `player/dog/Idle (4).png`);
    this.load.image(`player_dog_idle_5`, `player/dog/Idle (5).png`);
    this.load.image(`player_dog_idle_6`, `player/dog/Idle (6).png`);
    this.load.image(`player_dog_idle_7`, `player/dog/Idle (7).png`);
    this.load.image(`player_dog_idle_8`, `player/dog/Idle (8).png`);
    this.load.image(`player_dog_idle_9`, `player/dog/Idle (9).png`);
    this.load.image(`player_dog_idle_10`, `player/dog/Idle (10).png`);
    this.load.image(`player_dog_run_1`, `player/dog/Run (1).png`);
    this.load.image(`player_dog_run_2`, `player/dog/Run (2).png`);
    this.load.image(`player_dog_run_3`, `player/dog/Run (3).png`);
    this.load.image(`player_dog_run_4`, `player/dog/Run (4).png`);
    this.load.image(`player_dog_run_5`, `player/dog/Run (5).png`);
    this.load.image(`player_dog_run_6`, `player/dog/Run (6).png`);
    this.load.image(`player_dog_run_7`, `player/dog/Run (7).png`);
    this.load.image(`player_dog_run_8`, `player/dog/Run (8).png`);
    this.load.image(`player_dog_jump_1`, `player/dog/Jump (1).png`);
    this.load.image(`player_dog_jump_2`, `player/dog/Jump (2).png`);
    this.load.image(`player_dog_jump_3`, `player/dog/Jump (3).png`);
    this.load.image(`player_dog_jump_4`, `player/dog/Jump (4).png`);
    this.load.image(`player_dog_jump_5`, `player/dog/Jump (5).png`);
    this.load.image(`player_dog_jump_6`, `player/dog/Jump (6).png`);
    this.load.image(`player_dog_jump_7`, `player/dog/Jump (7).png`);
    this.load.image(`player_dog_jump_8`, `player/dog/Jump (8).png`);
    this.load.image(`player_dog_fall_1`, `player/dog/Fall (1).png`);
    this.load.image(`player_dog_fall_2`, `player/dog/Fall (2).png`);
    this.load.image(`player_dog_fall_3`, `player/dog/Fall (3).png`);
    this.load.image(`player_dog_fall_4`, `player/dog/Fall (4).png`);
    this.load.image(`player_dog_fall_5`, `player/dog/Fall (5).png`);
    this.load.image(`player_dog_fall_6`, `player/dog/Fall (6).png`);
    this.load.image(`player_dog_fall_7`, `player/dog/Fall (7).png`);
    this.load.image(`player_dog_fall_8`, `player/dog/Fall (8).png`);
    this.load.image(`bg_orange`, `backgrounds/background01.png`);
    this.load.image(`bg_night`, `backgrounds/background_0.png`);
    this.load.image(`apple`, `utils/Apple.png`);
    this.load.image(`pause`, `utils/pause.png`);
    this.load.image(`back`, `utils/back.png`);

    this.load.audio(`theme`, `music/theme_music.wav`);
    this.load.audio(`running`, `music/step_mud.wav`);
    this.load.audio(`jump`, `music/jump.wav`);
    this.load.audio(`collect`, `music/coin_pickup.wav`);

    this.load.once(`complete`, () => {
      this.registry.set(`level`, 1);
      this.registry.set(`unlocked-levels`, 1);
      this.scene.start(`Menu`);
    });
  }
}
export default Preload;
