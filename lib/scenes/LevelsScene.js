import Phaser from "phaser";
export class LevelsScene extends Phaser.Scene {
  constructor(config) {
    super(`LevelsScene`);
    this.config = config;
  }

  create() {
    this.add
      .image(0, 0, `bg_night`)
      .setOrigin(0)
      .setScale(5.7);

    this.menu = [];
    const levels = this.registry.get(`unlocked-levels`);
    for (let i = 1; i <= levels; i++) {
      this.menu.push({
        text: `Level ${i}`,
        level: i
      });
    }

    const spaceY = 42;
    let rowItemNr = 0;
    const widthThird = this.config.width / 3;
    this.menu.forEach((menuItem, index) => {
      const menuPosition = [index <= 4 ? widthThird : widthThird * 2, this.config.height / 2 + rowItemNr * spaceY];
      menuItem.object = this.add
        .text(...menuPosition, menuItem.text, { fontSize: `32px`, fill: `#fff` })
        .setOrigin(0.5, 1);

      menuItem.object.setInteractive();
      menuItem.object.on(`pointerover`, () => {
        menuItem.object.setStyle({ fill: `#ff0` });
      });
      menuItem.object.on(`pointerout`, () => {
        menuItem.object.setStyle({ fill: `#fff` });
      });

      menuItem.object.on(`pointerup`, () => {
        this.registry.set(`level`, menuItem.level);
        this.scene.start(`PlayScene`);
      });
      rowItemNr = index === 4 ? 0 : ++rowItemNr;
    });
  }
}
